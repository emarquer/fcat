# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import numpy as np
from dataset import Collate, get_dataset, LatticeDataset
from boa_vae import *
from datetime import datetime
import matplotlib.pyplot as plt
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]


# %%
class ConceptNumberUpperBoundPredictor(nn.Module):
    """Takes the whole attribute embeddings and the size predicted by the predictor used in the metric learning process, and predicts an upper bound for the number of concept."""
    def __init__(self, emb_size, reduction="mean"):
        super().__init__()
        self.emb_size = emb_size
        self.reduction = reduction
        self.predictor = nn.Sequential(
            nn.Linear(self.emb_size + 1, 256),
            nn.ReLU(),
            nn.Linear(256, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, 32),
            nn.ReLU(),
            nn.Linear(32, 1),
            nn.ReLU()
        )
    
    def forward(self, a_embeddings, metric_predictor_output):
        """
        :param a_embeddings: [batch x num_a x emb_size] the embeddings of the attributes
        :param metric_predictor_output: [batch] the number of concept predicted by the predictor used in the metric learning process
        """
        # a_embeddings: [batch x num_a x emb_size]
        if self.reduction == "mean":
            reduced = a_embeddings.mean(dim=-2) # [batch x emb_size]
        else:
            reduced = a_embeddings.max(dim=-2)[0] # [batch x emb_size]
        input = torch.cat([reduced, metric_predictor_output.view(-1, 1)], dim = -1)
        pred = self.predictor(input) # [batch x 1]

        return pred.view(-1)

def length_loss(input, target):
    error = input - target
    # error > 0 ~> input above target: error kept as is
    # error < 0 ~> input under target: error majored

    error = input - (target * 1.1)
    mask = error < 0
    error[mask] *= 10

    # squared error
    sq_error = error.pow(2)

    return sq_error.mean()


if __name__ == "__main__":
    # %%
    # Hyperparameters
    device="cuda"
    # BoA
    #boa_data = torch.load("../boa/boa_vae_e500_lr3schedule_b0001_cointent.tch", map_location=device)
    #emb_size = 124
    #boa_data.keys()


    # %%
    # LSTM
    batch_size = 8
    shuffle = True
    num_workers = 8
    lr = 1e-5
    epochs = 100 #300
    emb_size = 124


    # %%
    # dataset
    import os
    if os.path.isdir("latticenn/random-lattice-dataset/small_train/preprocessed"):
        train_dataset, dev_dataset = get_dataset("latticenn/random-lattice-dataset/small_train/preprocessed")
        eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/small_test/preprocessed")
        boa_data = torch.load("models/boa.tch", map_location=device)
    else:
        train_dataset, eval_dataset = get_dataset("../../random-lattice-dataset/preprocessed")
        #train_dataset, eval_dataset = get_dataset("../random-lattice-dataset/minisample/preprocessed")
        dev_dataset = eval_dataset
        boa_data = torch.load("../boa/boa_vae_e500_lr3schedule_b0001_cointent.tch", map_location=device)

    collate = Collate(DataAugment(0.1), masks=False)

    # create dataloader
    train_dataloader = DataLoader(train_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    dev_dataloader = DataLoader(dev_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    eval_dataloader = DataLoader(eval_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)


    # %%
    # reset random seed
    torch.manual_seed(0)

    size_predictor = ConceptNumberUpperBoundPredictor(emb_size).to(device)

    optim = torch.optim.Adam(size_predictor.parameters(), lr)
    print("model parameters", sum(size for param in size_predictor.parameters() for size in param.size()))

    for epoch in range(epochs):
        losses, dev_losses = [], []
        # train
        for i, (contexts, intents, stops, links, sizes) in enumerate(train_dataloader):
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)

            # compute embeddings
            with torch.no_grad():
                a_emb, mu, logvar = boa_data['encoder'](contexts)
                #o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
                pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])

            predicted_size = size_predictor(a_emb, pred_size)

            loss = length_loss(predicted_size, sizes.float())
            losses.append(loss.cpu().item())

            optim.zero_grad()
            loss.backward()
            optim.step()
        
        # dev
        for i, (contexts, intents, stops, links, sizes) in enumerate(dev_dataloader):
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)

            # compute embeddings
            with torch.no_grad():
                a_emb, mu, logvar = boa_data['encoder'](contexts)
                #o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
                pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
                predicted_size = size_predictor(a_emb, pred_size)
                loss = length_loss(predicted_size, sizes.float())
                dev_losses.append(loss.cpu().item())

        print(f"{time()} [{epoch + 1}/{epochs}]: train: {sum(losses)/len(losses):7.3f}, dev: {sum(dev_losses)/len(dev_losses):7.3f}")

    torch.save(size_predictor.cpu(), "models/concept_upper_bound.tch")
    size_predictor = size_predictor.to(device)

    for i, (contexts, intents, stops, links, sizes) in enumerate(eval_dataloader):
        contexts = contexts.to(device)
        intents = intents.to(device)
        sizes = sizes.to(device)

        # compute embeddings
        with torch.no_grad():
            a_emb, mu, logvar = boa_data['encoder'](contexts)
            #o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
            pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
            predicted_size = size_predictor(a_emb, pred_size)

            plt.scatter(sizes.cpu(), predicted_size.cpu(), alpha = 0.1, color='blue')

    plt.plot([1, 150], [1, 150], label='perfect predictor (y=x)', color='orange')
    plt.xlabel("true number of concepts")
    plt.ylabel("predicted number of concepts")
    plt.legend()
    plt.savefig("models/concept_upper_bound.png")

