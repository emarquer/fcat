# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %%
import torch
import torch.nn as nn
from torch.utils.data import DataLoader
import numpy as np
from dataset import Collate, get_dataset, LatticeDataset
from boa_vae import *
from concept_number import *
from eval import *
from datetime import datetime
import matplotlib.pyplot as plt
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]


class MultiHeadAttention(nn.Module):
    def __init__(self, key_size, value_size, num_heads=1, sigmoid_weights=False):
        super().__init__()
        
        self.key_size = key_size
        self.value_size = value_size
        self.num_heads = num_heads
        self.key_to_querries = nn.Linear(self.key_size, self.value_size * self.num_heads)
        self.value_projection = nn.Linear(self.value_size, self.value_size)
        self.softmax = nn.Softmax(dim=-2)
        self.sigmoid_weights = sigmoid_weights
        self.sigmoid = nn.Sigmoid()

    def forward(self, key, values):
        # key: [batch x key_size]
        # values: [batch x num_value x value_size]

        # compute querries and projections
        querries = self.key_to_querries(key) # [batch x value_size * num_heads]
        querries = querries.view(values.size(0), self.value_size, self.num_heads)  # [batch x value_size x num_heads]
        projections = self.value_projection(values) # [batch x num_value x value_size]

        # compute attention weights
        weights = torch.bmm(projections, querries) # [batch x num_value x num_heads]
        if self.sigmoid_weights:
            softmax_weights = self.softmax(self.sigmoid(weights))
        else:
            softmax_weights = self.softmax(weights)
        # softmax_weights: [batch x num_value x num_heads]

        # compute attention summary
        projections = projections.expand(self.num_heads, *projections.size()) # [num_heads x batch x num_value x value_size]
        projections = projections.permute(1, 2, 0, 3) # [batch x num_value x num_heads x value_size]
        softmax_weights = softmax_weights.expand(self.value_size, *softmax_weights.size()) # [value_size x batch x num_value x num_heads]
        softmax_weights = softmax_weights.permute(1, 2, 3, 0) # [batch x num_value x num_heads x value_size]
        summary = (projections * softmax_weights.view(softmax_weights.size())).sum(dim=1) # [batch x num_heads x value_size]

        # summary: [batch x num_heads x value_size]
        # weights: [batch x num_value x num_heads]
        return summary, weights

class SingleDirConceptGenerator(nn.Module):
    def __init__(self, emb_size, attribute_summary_size, hidden_size, num_layers=1, heads=1, dropout=0.1, reduction="mean"):
        super().__init__()
        self.reduction = reduction

        self.num_layers = num_layers
        self.emb_size = emb_size
        self.hidden_size = hidden_size
        self.attribute_summary_size = attribute_summary_size
        self.key_size = self.num_layers * self.hidden_size * 2
        self.heads = heads
        self.lstm_input_size = self.emb_size * (self.heads * 2 + 2) + self.attribute_summary_size + 2
        
        self.attribute_dan = nn.Sequential(
            nn.Linear(self.emb_size, 128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.ReLU(),
            nn.Linear(128, 128),
            nn.ReLU(),
            nn.Linear(128, 64),
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Linear(64, 64),
            nn.ReLU(),
            nn.Linear(64, self.attribute_summary_size),
            nn.ReLU()
        )
        
        self.attribute_attention = MultiHeadAttention(self.key_size, self.emb_size, num_heads=self.heads)
        self.object_attention = MultiHeadAttention(self.key_size, self.emb_size, num_heads=self.heads)
        self.self_attention = MultiHeadAttention(self.key_size, self.emb_size, num_heads=2, sigmoid_weights=True)
        
        self.lstm = nn.LSTM(self.lstm_input_size, self.hidden_size, num_layers=num_layers, batch_first=True)
        self.lstm_output = nn.Sequential(nn.ReLU(), nn.Linear(self.hidden_size, self.emb_size))#, nn.Sigmoid())
        
        self.dropout = nn.Dropout(dropout)

        def init_weights(m):
            if "weight" in dir(m):
                torch.nn.init.kaiming_normal_(m.weight, nonlinearity='relu')
        self.apply(init_weights)

    def forward(self, a_emb, o_emb, predicted_concept_number, max_concept_number=None):
        self.lstm.flatten_parameters()

        # a_emb: [batch x num_a x emb_size]
        # o_emb: [batch x num_o x emb_size]
        # predicted_concept_number: [batch]
        # max_concept_number: integer
        batch = a_emb.size(0)
        if max_concept_number is None:
            max_concept_number = predicted_concept_number.max()[0]
        num_c = max_concept_number
        
        # attribute summary
        if self.reduction == "mean":
            reduced = a_emb.mean(dim=-2) # [batch x emb_size]
        else:
            reduced = a_emb.max(dim=-2)[0] # [batch x emb_size]
        a_summary = self.attribute_dan(reduced) # [batch x emb_size]
        a_summary = a_summary.expand(max_concept_number, batch, -1) # [max_concept_number x batch x emb_size]
        a_summary = a_summary.transpose(1, 0) # [batch x max_concept_number x emb_size]
        #a_summary = self.dropout(a_summary)

        # attribute summary + indexes
        indices = torch.arange(max_concept_number, device=a_summary.device).float() # [max_concept_number]
        indices = indices.expand(batch, max_concept_number) # [batch x max_concept_number]
        indices = indices.view(batch, max_concept_number, 1) # [batch x max_concept_number x 1]
        predicted_concept_number = predicted_concept_number.expand(1, max_concept_number, batch)  # [1 x max_concept_number x batch]
        predicted_concept_number = predicted_concept_number.transpose(0, -1)  # [batch x max_concept_number x 1]
        a_summary = torch.cat([indices, predicted_concept_number, a_summary], dim = -1) # [batch x max_concept_number x emb_size + 2]

        hidden_and_cell = [torch.zeros(self.num_layers, batch, self.hidden_size, device=a_emb.device)] * 2
        concepts = []
        cover_adjacencies = []
        order_adjacencies = []
        for i in range(num_c): # process each concept one after the other, due to self attention
            # compute attentions
            key = torch.cat([x.transpose(1, 0).reshape(batch, -1) for x in hidden_and_cell], dim=-1)
            #key = self.dropout(key)
            a_attention, _ = self.attribute_attention(key, a_emb)
            o_attention, _ = self.object_attention(key, o_emb)
            if i > 0:
                s_attention, weights = self.self_attention(key, torch.cat(concepts, dim=-2))
                # s_attention: [batch x 2 x emb_size]
                # weights: [batch x num_value x 2]
                cover_adjacencies.append(weights[:,:,0])
                order_adjacencies.append(weights[:,:,1])
            else:
                s_attention = torch.zeros([batch, 2, self.emb_size], device=o_attention.device)

            # prepare LSTM input
            a_attention = a_attention.view(batch, self.emb_size * self.heads)
            o_attention = o_attention.view(batch, self.emb_size * self.heads)
            s_attention = s_attention.view(batch, self.emb_size * 2)
            input = torch.cat([a_attention, o_attention, s_attention, a_summary[:, i]], dim = -1)
            input = input.view(batch, 1, -1)
            input = self.dropout(input)
            # [batch x self.key_size * 3 + self.skeleton_size]

            # LSTM step
            hidden_and_cell = [self.dropout(h) for h in hidden_and_cell]
            output, hidden_and_cell = self.lstm(input, hidden_and_cell)
            concepts.append(self.lstm_output(output))

        return torch.cat(concepts, dim=-2), torch.cat(cover_adjacencies, dim=-1), torch.cat(order_adjacencies, dim=-1)


# %%
class ConceptGenerator(nn.Module):
    """bidirectional concept generator"""
    def __init__(self, emb_size, attribute_summary_size, hidden_size, num_layers=1, heads=1):
        super().__init__()

        self.generator = SingleDirConceptGenerator(emb_size, attribute_summary_size, hidden_size, num_layers=num_layers, heads=heads)
        self.generator_reverse = SingleDirConceptGenerator(emb_size, attribute_summary_size, hidden_size, num_layers=num_layers, heads=heads)
        self.output = nn.Linear(emb_size * 2, emb_size)

    def forward(self, skeleton, a_emb, o_emb):
        # skeleton: [batch x num_c x skeleton_size]
        # a_emb: [batch x num_a x emb_size]
        # o_emb: [batch x num_o x emb_size]
        concepts = self.generator(skeleton, a_emb, o_emb)
        concepts_reverse = self.generator_reverse(skeleton.flip(-2), a_emb, o_emb)
        concepts_reverse = concepts_reverse.flip(-2)

        concepts = self.output(torch.cat([concepts, concepts_reverse], dim=-1))
        return concepts


if __name__ == "__main__":
    # %%
    # Hyperparameters
    device="cuda" if torch.cuda.is_available() else "cpu"
    # BoA
    emb_size = 124
    # LSTM
    attribute_summary_size = 64
    hidden_size = 128
    heads = 4
    layers = 4
    batch_size = 8
    shuffle = True
    num_workers = 8
    lr = 1e-3#2
    dropout = 0.25
    epochs = 30 #300
    betas = 0.9, 0.999

    # %%
    # dataset
    import os
    if os.path.isdir("latticenn/random-lattice-dataset/small_train/preprocessed"):
        train_dataset, dev_dataset = get_dataset("latticenn/random-lattice-dataset/small_train/preprocessed")
        eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/small_test/preprocessed")
        boa_data = torch.load("models/boa.tch", map_location=device)
        size_predictor = torch.load("models/concept_upper_bound.tch", map_location=device)
    else:
        train_dataset, eval_dataset = get_dataset("../../random-lattice-dataset/preprocessed")
        dev_dataset = eval_dataset
        boa_data = torch.load("../boa/boa_vae_e500_lr3schedule_b0001_cointent.tch", map_location=device)
        size_predictor = torch.load("concept_upper_bound.tch", map_location=device)
    collate = Collate(DataAugment(0.1), masks=False)


    # create dataloader
    train_dataloader = DataLoader(train_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate)
    dev_dataloader = DataLoader(dev_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate)
    eval_dataloader = DataLoader(eval_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate)


    # %%
    # reset random seed
    torch.manual_seed(0)

    concept_generator = SingleDirConceptGenerator(emb_size, attribute_summary_size, hidden_size, num_layers=layers, heads=heads, dropout=dropout).to(device)
    
    def prod(iterable):
        p = 1
        for x in iterable:
            p *= x
        return p
    print("concept model parameters", sum(prod(param.size()) for param in concept_generator.parameters()))
    
    mse = nn.MSELoss()
    def weighted_bce(x, y):
        w = (1 - y).sum().float()/y.sum() # weight of positive examples
        weights = torch.ones_like(y)
        weights[y.bool()] = w
        weights /= weights.mean()
        return nn.functional.binary_cross_entropy_with_logits(x, y, weights)    
    
    def normalized_mse(pred, target):
        mean = torch.mean(target)
        std = torch.std(target)
        pred = (pred - mean) / std
        target = (target - mean) / std
        return mse(10 * pred, 10 * target)
    bce_criterion = nn.BCEWithLogitsLoss()
    #bce_criterion = nn.BCELoss()
    #bce_criterion = weighted_bce
    mse_criterion = mse
    l1 = nn.L1Loss()
    #mse_criterion = normalized_mse
    
    # Remove sigmoid from decoder, to use BCEWithLogitsLoss, as a patch for some gradient zeroing problems due to the sigmoid
    decoder = boa_data['decoder']
    decoder.oa_decoder = decoder.oa_decoder[:-1]

    optim = torch.optim.Adam([
        {'params': concept_generator.parameters()},
    ], lr, betas=betas)
    
    def clip_grad(max_norm=1e6):
        nn.utils.clip_grad.clip_grad_norm_(concept_generator.parameters(), max_norm)
    
    def dev_performance(epoch=-1, losses=[]):
        records = []
        cover_records = []
        order_records = []
        dev_losses = []
        for i, (contexts, intents, stops, links, sizes) in enumerate(dev_dataloader):
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)
            cover_adjacencies = links[:,:,0].to(device)
            order_adjacencies = links[:,:,1].to(device)

            # compute embeddings
            with torch.no_grad():
                a_emb, mu, logvar = boa_data['encoder'](contexts)
                a_emb = mu
                o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
                c_emb = boa_data['encoder'].encode_objects(intents, a_emb)
                pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
                predicted_size = size_predictor(a_emb, pred_size)
                
                pred_c_emb, pred_cover_adjacencies, pred_order_adjacencies = concept_generator(a_emb, o_emb, predicted_size, stops.size(1))
                pred_intents = decoder(a_emb, pred_c_emb)
                
                mse_loss = mse_criterion(pred_c_emb, c_emb)
                loss = (
                    0.1 * bce_criterion(pred_intents, intents) +
                    bce_criterion(pred_cover_adjacencies, cover_adjacencies) +
                    bce_criterion(pred_order_adjacencies, order_adjacencies) +
                    mse_loss)
                dev_losses.append(loss.cpu().item())
            
            fixed_threshold = 0.5
            # AUC ROC
            # intents
            try:
                pred_intents = torch.sigmoid(pred_intents)
                threshold = find_optimal_threshold(pred_intents.cpu(), intents.cpu())
                
                auc = auc_roc(pred_intents.cpu(), intents.cpu())
                records += [{**scores, 'threshold': fixed_threshold, 'AUC ROC': auc, 'failure rate': 0} for scores in  precision_recall_f1_miss_false_discovery(pred_intents.cpu() > fixed_threshold, intents.cpu())]
            except ValueError:
                records += [{'failure rate': 1}]
            
            # cover relation
            try:
                pred_cover_adjacencies = torch.sigmoid(pred_cover_adjacencies)
                auc = auc_roc(pred_cover_adjacencies.cpu(), cover_adjacencies.cpu())
                cover_records.append({'AUC ROC': auc, 'failure rate': 0})
            except ValueError:
                records += [{'failure rate': 1}]
            
            # order relation
            try:
                pred_order_adjacencies = torch.sigmoid(pred_order_adjacencies)
                auc = auc_roc(pred_order_adjacencies.cpu(), order_adjacencies.cpu())
                order_records.append({'AUC ROC': auc, 'failure rate': 0})
            except ValueError:
                records += [{'failure rate': 1}]
            
        print(len(dev_losses), len(losses))
        if epoch < 0:
            print(f"{time()}: start training, dev: {sum(dev_losses)/len(dev_losses):7.3f}")
        else:
            print(f"{time()} [{epoch + 1}/{epochs}]: train: {sum(losses)/len(losses):7.3f}, dev: {sum(dev_losses)/len(dev_losses):7.3f}")
        print("concepts")
        df = pd.DataFrame.from_records(records)
        print(df.mean().apply(lambda x: f'{x:.3f}') + " +- " + df.std().apply(lambda x: f'{x:.3f}'))
        print("cover relation")
        df = pd.DataFrame.from_records(cover_records)
        print(df.mean().apply(lambda x: f'{x:.3f}') + " +- " + df.std().apply(lambda x: f'{x:.3f}'))
        print("order relation")
        df = pd.DataFrame.from_records(order_records)
        print(df.mean().apply(lambda x: f'{x:.3f}') + " +- " + df.std().apply(lambda x: f'{x:.3f}'))

    dev_performance()
    # train
    for epoch in range(epochs):
        losses = []
        for i, (contexts, intents, stops, links, sizes) in enumerate(train_dataloader):
            contexts = contexts.to(device)
            intents = intents.to(device)
            stops = stops.to(device)
            cover_adjacencies = links[:,:,0].to(device)
            order_adjacencies = links[:,:,1].to(device)

            # compute embeddings
            with torch.no_grad():
                a_emb, mu, logvar = boa_data['encoder'](contexts)
                o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
                c_emb = boa_data['encoder'].encode_objects(intents, a_emb)
                pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
                predicted_size = size_predictor(a_emb, pred_size)

            # generate
            pred_c_emb, pred_cover_adjacencies, pred_order_adjacencies = concept_generator(a_emb, o_emb, predicted_size, stops.size(1))
            pred_intents = decoder(a_emb, pred_c_emb)
            
            mse_loss = mse_criterion(pred_c_emb, c_emb)
            loss = (
                0.1 * bce_criterion(pred_intents, intents) +
                bce_criterion(pred_cover_adjacencies, cover_adjacencies) +
                bce_criterion(pred_order_adjacencies, order_adjacencies) +
                mse_loss)
            
            losses.append(loss.cpu().item())
            optim.zero_grad()
            loss.backward()
            clip_grad()
            if i == 0:
                print("mse", mse_criterion(pred_c_emb, c_emb).cpu().item())
                print("concept (clip)", sum(param.grad.norm() for param in concept_generator.parameters()))
            optim.step()

        dev_performance(epoch, losses)
        
        torch.save({
            'concept_generator': concept_generator, 
            'decoder': decoder,
            'optim': optim}, "models/concept.tch")
