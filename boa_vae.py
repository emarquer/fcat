# To add a new cell, type '# %%'
# To add a new markdown cell, type '# %% [markdown]'
# %% [markdown]
# # Bag of attributes
# 
# positional encoding
# no more rnn pre-encoder: replaced by attribute pre-encoder, negative/positive application, and random key
# alternative

# %%
import numpy as np
import pickle
import torch
import torch.nn as nn
from torch.utils.data import Dataset, DataLoader
from torch import save as torch_save, load as torch_load, Tensor
from pprint import pprint
from dataset import get_dataset, Collate, LatticeDataset, DataAugment
import typing as t
from math import exp
from datetime import datetime
time = lambda: datetime.now().strftime("%H:%M:%S.%f")[:-3]


# %%
class BOA_encoder(nn.Module):
    def __init__(self, emb_size=32, pre_emb_size=4, layers=2, reduction="mean", object_reduction="max"):
        super().__init__()
        self.reduction = reduction
        self.object_reduction = object_reduction
        self.pre_emb_size = pre_emb_size
        self.emb_size = emb_size * 2
        self.layers = layers
        self.object_preencoder = nn.LSTM(1, self.pre_emb_size, num_layers=self.layers, bidirectional=True, batch_first=True)
        self.attribute_encoder_rnn = nn.LSTM(self.pre_emb_size * 2, self.emb_size, num_layers=self.layers, bidirectional=True, batch_first=True)
        self.attribute_encoder_linear = nn.Sequential(
            nn.Linear(2 * (self.emb_size * 2 * self.layers) + self.emb_size, 4 * self.emb_size),
            nn.ReLU(),
            nn.Linear(4 * self.emb_size, 2 * self.emb_size),
            nn.ReLU(),
            nn.Linear(2 * self.emb_size, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, self.emb_size//2),#new
            nn.ReLU(),
            nn.Linear(self.emb_size//2, self.emb_size)#new
        )

    def flatten_parameters(self):
        self.object_preencoder.flatten_parameters()
        self.attribute_encoder_rnn.flatten_parameters()

    def forward(self, input):
        #self.flatten_parameters()
        batch, num_o, num_a = input.size()
        # input: [batch x num_o x num_a]
        
        # --- step 1: encode the attributes wrt. other attributes in each object ---
        # input: [batch x num_o x num_a]
        preencoded_o, _ = self.object_preencoder(input.view(batch * num_o, num_a, 1))
        # preencoded_objects: [batch * num_o x num_a x 2 * emb_size]
        if self.reduction == 'max':
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).max(dim=-2)[0]
        else:
            preencoded_o = preencoded_o.view(batch, num_o, num_a, 2, self.pre_emb_size).mean(dim=-2)
        preencoded_o = preencoded_o.view(batch, num_o, num_a, self.pre_emb_size)
        # preencoded_objects: [batch x num_o x num_a x emb_size]
        # boa attribute encoder
        encoded_a_list = []
        for i in range(num_a):
            a = preencoded_o[:, :, i]
            not_a = preencoded_o[:, :, :i].sum(dim=-2) + preencoded_o[:, :, i+1:].sum(dim=-2)
            # a, not_a: [batch x num_o x emb_size]
            o, (h_n, c_n) = self.attribute_encoder_rnn(torch.cat([a, not_a], dim = -1))
            if self.reduction == 'max':
                o_mean = o.view(batch, num_o, 2, self.emb_size).max(dim=-2)[0].max(dim=-2)[0]
            else:
                o_mean = o.view(batch, num_o, 2, self.emb_size).mean(dim=-2).mean(dim=-2)
            h_n = h_n.view(batch, -1)
            c_n = c_n.view(batch, -1)
            # o: [batch x emb_size]
            # h_n, c_n: [batch x 2 * layers * emb_size]
            encoded_a = self.attribute_encoder_linear(torch.cat([h_n, c_n, o_mean], dim=-1))
            # encoded_a: [batch x 2 x emb_size]
            encoded_a_list.append(encoded_a)
        a_embeddings = torch.stack(encoded_a_list, dim=-2)
        # a_embeddings: [batch x num_a x emb_size]

        a_embeddings, mu, logvar = self.reparameterize(a_embeddings)

        return a_embeddings, mu, logvar

    def reparameterize(self, h):
        mu, logvar = h.chunk(2, dim=-1)
        std = torch.exp(0.5 * logvar)
        eps = torch.randn_like(std)
        return mu + eps * std, mu, logvar

    def encode_objects(self, input, a_embeddings):
        """ encode the objects wrt. their attributes and the embedding """
        batch, num_o, num_a = input.size()
        _, __, emb_size = a_embeddings.size()
        # input: [batch x num_o x num_a]
        
        oa_embeddings = torch.stack([input] * emb_size, dim=-1) * a_embeddings.view(batch, 1, num_a, emb_size)
        # oa_embeddings: [batch x num_o x num_a x emb_size]
        if self.object_reduction == 'max':
            o_embeddings = oa_embeddings.max(dim=-2)[0]
        else:
            o_embeddings = oa_embeddings.mean(dim=-2)
        # o_embeddings: [batch x num_o x emb_size]

        return o_embeddings


# %%
class BOA_decoder(nn.Module):
    def __init__(self, emb_size=16):
        super().__init__()
        self.emb_size = emb_size
        self.oa_decoder = nn.Sequential(
            nn.Linear(self.emb_size * 2, self.emb_size * 2),
            nn.ReLU(),
            nn.Linear(self.emb_size * 2, self.emb_size * 2),
            nn.ReLU(),
            nn.Linear(self.emb_size * 2, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, 1),
            nn.Sigmoid()
        )

    def forward(self, a_embeddings, o_embeddings):
        batch, num_a, _ = a_embeddings.size()
        batch, num_o, _ = o_embeddings.size()
        # a_embeddings: [batch x num_a x emb_size]
        # o_embeddings: [batch x num_o x emb_size]
        
        o_embeddings_ = o_embeddings.expand(num_a, batch, num_o, self.emb_size).permute(1, 2, 0, 3)
        a_embeddings_ = a_embeddings.expand(num_o, batch, num_a, self.emb_size).transpose(0, 1)
        # a_embeddings_: [batch x num_o x num_a x emb_size]
        # o_embeddings_: [batch x num_o x num_a x emb_size]
        oa_embeddings = torch.cat([a_embeddings_, o_embeddings_], dim=-1)
        
        pred = self.oa_decoder(oa_embeddings).squeeze(-1)
        # pred: [batch x num_o x num_a]
        return pred

class SimilarityPredictor(nn.Module):
    def __init__(self, emb_size):
        super().__init__()
        self.emb_size = emb_size
        self.predictor = nn.Sequential(
            nn.Linear(self.emb_size * 2, self.emb_size),
            nn.ReLU(),
            nn.Linear(self.emb_size, 1),
            nn.Sigmoid()
        )
    
    def forward(self, a_embeddings):
        batch, num_a, emb_size = a_embeddings.size()
        # a_embeddings: [batch x num_a x emb_size]
        a_embeddings_stacked = a_embeddings.expand(num_a, batch, num_a, emb_size).transpose(0, 1).float() # [batch x num_a x num_a' x emb_size]
        # exchange attribute and stack dimensions
        a_embeddings_stacked_transposed = a_embeddings_stacked.transpose(1, 2) # [batch x num_a' x num_a x emb_size]

        x = torch.cat([a_embeddings_stacked, a_embeddings_stacked_transposed], dim = -1) # [batch x num_a' x num_a x 2 * emb_size]

        pred = self.predictor(x) # [batch x num_a' x num_a x 1]
        pred = pred.view(pred.size()[:-1]) # [batch x num_a' x num_a]

        return pred

class LengthPredictor(nn.Module):
    def __init__(self, emb_size, reduction="mean"):
        super().__init__()
        self.emb_size = emb_size
        self.reduction = reduction
        self.predictor = nn.Sequential(
            nn.Linear(self.emb_size, 256),
            nn.ReLU(),
            nn.Linear(256, 1),
            nn.ReLU()
        )
    
    def forward(self, a_embeddings):
        # a_embeddings: [batch x num_a x emb_size]
        if self.reduction == "mean":
            reduced = a_embeddings.mean(dim=-2) # [batch x emb_size]
        else:
            reduced = a_embeddings.max(dim=-2)[0] # [batch x emb_size]
        pred = self.predictor(reduced) # [batch x 1]

        return pred.view(-1)

# %%
def attribute_co_intent_similarity(intents, epsilon=1e-8):
    # co_occurence_matrix[i,j] = number of time i & j appear in the same intent
    co_occurence_matrix = torch.matmul(intents.transpose(-1, -2), intents)
    occurence_vector = intents.sum(dim=-2)
    # appearance_matrix[i,j] = #i + #j + epsilon
    if intents.dim() == 3: # batch variant
        occurence_matrix = occurence_vector.expand(co_occurence_matrix.size(-1), *occurence_vector.size()).transpose(1, 0)
        appearance_matrix = occurence_matrix.transpose(-1, -2) + occurence_matrix + epsilon
    else:
        appearance_matrix = occurence_vector.expand(co_occurence_matrix.size()).transpose(-1, -2) + occurence_vector + epsilon
    # co-occurence similarity between i and j:
    # 2 * #intents containing both i&j / (#intents containing both i + #intents containing both j + epsilon)
    # (2 * co_occurence_matrix[i,j].float()) / (intents[:,i].sum() + intents[:,j].sum() + epsilon).float()
    co_occurence_similarity_matrix = 2 * co_occurence_matrix.float() / appearance_matrix
    # if the sum of the individual occurences are 0, it means that both attributes are empty, and thus a particular case of similarity
    co_occurence_similarity_matrix += (appearance_matrix <= epsilon).float() 
    return co_occurence_similarity_matrix
def attribute_co_intent_distance(intents, epsilon=1e-8):
    # co_occurence_matrix[i,j] = number of time i appears without j in the same intent
    no_occurence_matrix = torch.matmul(intents.transpose(-1, -2), 1 - intents)
    occurence_vector = intents.sum(dim=-2)
    # appearance_matrix[i,j] = #i + #j + epsilon
    if intents.dim() == 3: # batch variant
        occurence_matrix = occurence_vector.expand(no_occurence_matrix.size(-1), *occurence_vector.size()).transpose(1, 0)
        appearance_matrix = occurence_matrix.transpose(-1, -2) + occurence_matrix + epsilon
    else:
        appearance_matrix = occurence_vector.expand(no_occurence_matrix.size()).transpose(-1, -2) + occurence_vector + epsilon
    # co-occurence distance between i and j:
    # #intents containing i but not j + #intents containing j but not i / (#intents containing both i + #intents containing both j + epsilon)
    # (2 * co_occurence_matrix[i,j].float()) / (intents[:,i].sum() + intents[:,j].sum() + epsilon).float()
    co_occurence_distance_matrix = (no_occurence_matrix + no_occurence_matrix.transpose(-1, -2)) / appearance_matrix
    return co_occurence_distance_matrix   
def attribute_cosine_similarity(a_embeddings):
    source = a_embeddings.expand(a_embeddings.size(-2), *a_embeddings.size()).float()
    target = source.transpose(0, -2) # exchange attribute and duplicate dimensions
    pairwise_cosine_similarity = torch.cosine_similarity(source, target, dim=-1)
    if a_embeddings.dim() == 3: # batch variant
        pairwise_cosine_similarity = pairwise_cosine_similarity.transpose(0, 1)
    return pairwise_cosine_similarity


# %%
def kl_divergence(mu, logvar):
    # see Appendix B from VAE paper:
    # Kingma and Welling. Auto-Encoding Variational Bayes. ICLR, 2014
    # https://arxiv.org/abs/1312.6114
    # 0.5 * sum(1 + log(sigma^2) - mu^2 - sigma^2)
    KLD = -0.5 * torch.sum(1 + logvar - mu.pow(2) - logvar.exp())
    return KLD


if __name__=="__main__":
    import sys
    mode_metric_learn = len(sys.argv) > 1 # metric learn if an argument is provided

    # %%
    batch_size = 8
    shuffle = True
    num_workers = 8
    lr = 1e-3
    weight_decay = 0
    epochs = 500
    device= 'cuda'
    emb_size = 124#64
    pre_emb_size = 64
    att_cointent_slice = slice(0, emb_size//2)
    sizes_slice = slice(emb_size//2, emb_size//2 + emb_size//4)
    beta = 1e-4
    name = "boa_vae_e500_lr3schedule_b0001"
    name_cointent = "boa_vae_b00001_cointent"
    size_weight = lambda epoch_: min(epoch_ / (epochs / 2), 1)
    att_cointent_weight = lambda epoch_: min(epoch_ / (epochs / 2), 1) * 100
    # annealing = 1 / (1 + exp(slope * (-x + center)))
    slope = 2
    center = 50
    annealing = lambda epoch_: 1. / (1. + exp(slope * (-epoch_ + center)))
    
    

    import os
    if os.path.isdir("latticenn/random-lattice-dataset/train/preprocessed"):
        train_dataset = LatticeDataset("latticenn/random-lattice-dataset/train/preprocessed")
        eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/test/preprocessed")
        
        train_dataset, dev_dataset = get_dataset("latticenn/random-lattice-dataset/small_train/preprocessed")
        eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/small_test/preprocessed")
        # latticenn/random-lattice-dataset/small_train/preprocessed/5x5/00502.pkl latticenn/random-lattice-dataset/small_train/preprocessed/5x5/00337.pkl latticenn/random-lattice-dataset/small_train/preprocessed/5x5/00475.pkl have problems

    else:
        train_dataset, eval_dataset = get_dataset("../random-lattice-dataset/preprocessed")
        dev_dataset = eval_dataset
        #train_dataset, eval_dataset = get_dataset("../random-lattice-dataset/minisample/preprocessed")

    if not mode_metric_learn:
        collate = Collate(DataAugment(0.1, 0.01))
    else:
        collate = Collate(DataAugment(0.1))

    # create dataloader
    train_dataloader = DataLoader(train_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    dev_dataloader = DataLoader(dev_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)
    eval_dataloader = DataLoader(eval_dataset,
                            batch_size=batch_size,
                            shuffle=shuffle,
                            num_workers=num_workers,
                            collate_fn=collate,
                            pin_memory=True)

    # create or load model
    if not mode_metric_learn:
        encoder = BOA_encoder(emb_size=emb_size, pre_emb_size=pre_emb_size).to(device)
        decoder = BOA_decoder(emb_size=emb_size).to(device)
        data = {"encoder": encoder, "decoder": decoder}
    else:
        data = torch.load(name + '.tch', map_location=device)
        encoder = data["encoder"]
        decoder = data["decoder"]
        similarity_predictor = SimilarityPredictor(emb_size=emb_size//2).to(device)
        length_predictor = LengthPredictor(emb_size=emb_size//4).to(device)
        data["similarity_predictor"] = similarity_predictor
        data["length_predictor"] = length_predictor
        data["att_cointent_slice"] = att_cointent_slice
        data["sizes_slice"] = sizes_slice
        
        name = name_cointent

    print(f"{time()} starting metric learning" if mode_metric_learn else f"{time()} initial training")
    
    logs = []
    
    def dev_performance(epoch):
        with torch.no_grad():
            losses = []
            kls = []
            att_cointent_losses = []
            sizes_losses = []
            for contexts, intents, stops, links, sizes in dev_dataloader:
                contexts = contexts.to(device) # [batch x num_o x num_a]
                intents = intents.to(device)
                sizes = sizes.to(device)

                a_emb, mu, logvar = encoder(contexts)
                o_emb = encoder.encode_objects(contexts, a_emb)
                pred = decoder(a_emb, o_emb)

                loss = bce_criterion(pred, contexts.float())
                losses.append(loss.detach().cpu().item())

                kl = kl_divergence(mu, logvar)
                kls.append(kl.detach().cpu().item())
                loss += kl * beta * annealing(epoch)

                if mode_metric_learn:
                    att_cointent_loss = mse_criterion(similarity_predictor(a_emb[:, :, att_cointent_slice]), attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * metric_weight(epoch)
                
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * metric_weight(epoch)
                    
            
            log = {"set": "dev", "epoch": epoch}
            log["reconstruction loss"] = sum(losses)/len(losses)
            log["kl divergence"] = sum(kls)/len(kls)
            log["kl annealing"] = annealing(epoch)
            if not mode_metric_learn:
                print(f"{time()} dev: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:9.5f} x {log['kl annealing']:.3f}")
            else:
                message = f"{time()} dev: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:15.5f} x {log['kl annealing']:.3f}"
                
                log["co-intent weight"] = att_cointent_weight(epoch)
                log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses)
                message += f", co-intent loss: {log['co-intent metric loss']:11.5f} x {log['co-intent weight']:.3f}"
                
                log["size weight"] = size_weight(epoch)
                log["size metric loss"] = sum(sizes_losses)/len(sizes_losses)
                message += f", size loss: {log['size metric loss']:11.5f} x {log['size weight']:.3f}"
                    
                print(message)
            return log
    # training loop
    bce_criterion = nn.BCELoss()
    mse_criterion = nn.MSELoss()
    optim = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()], lr, weight_decay=weight_decay)
    
    scheduler = torch.optim.lr_scheduler.ReduceLROnPlateau(optim, patience=20, verbose=True, eps=1e-7, factor=0.1, cooldown=20)
    
    if mode_metric_learn:
        optim_metric_similarity = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in similarity_predictor.parameters()], lr)
        optim_metric_length = torch.optim.Adam([x for x in encoder.parameters()] + [x for x in decoder.parameters()] + [x for x in length_predictor.parameters()], lr)
    
    for epoch in range(epochs):
        losses = []
        kls = []
        att_cointent_losses = []
        sizes_losses = []
        for contexts, intents, stops, links, sizes in train_dataloader:
            contexts = contexts.to(device)
            intents = intents.to(device)
            sizes = sizes.to(device)

            a_emb, mu, logvar = encoder(contexts)
            o_emb = encoder.encode_objects(contexts, a_emb)
            pred = decoder(a_emb, o_emb)

            loss = bce_criterion(pred, contexts.float())
            losses.append(loss.detach().cpu().item())

            kl = beta * kl_divergence(mu, logvar)
            kls.append(kl.detach().cpu().item())
            loss += kl * beta * annealing(epoch)

            if mode_metric_learn:
                if epoch % 2 == 0:
                    att_cointent_loss = mse_criterion(similarity_predictor(a_emb[:, :, att_cointent_slice]), attribute_co_intent_similarity(intents))
                    att_cointent_losses.append(att_cointent_loss.detach().cpu().item())
                    loss += att_cointent_loss * metric_weight(epoch)
                    optim_metric_similarity.zero_grad()
                    loss.backward()
                    optim_metric_similarity.step()
                else:
                    sizes_loss = mse_criterion(length_predictor(a_emb[:, :, sizes_slice]), sizes.float())
                    sizes_losses.append(sizes_loss.detach().cpu().item())
                    loss += sizes_loss * metric_weight(epoch)
                    optim_metric_length.zero_grad()
                    loss.backward()
                    optim_metric_length.step()
            else:
                optim.zero_grad()
                loss.backward()
                optim.step()

        if epoch > center: # only start after the annealing is in place
            scheduler.step(sum(losses)/len(losses) + sum(kls)/len(kls))
            
        if epoch%1 == 0 or epoch == epochs-1:
            log = {"set": "train", "epoch": epoch}
            log["reconstruction loss"] = sum(losses)/len(losses)
            log["kl divergence"] = sum(kls)/len(kls)
            log["kl annealing"] = annealing(epoch)
            if not mode_metric_learn:
                print(f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:9.5f} x {log['kl annealing']:.3f}")
            else:
                message = f"{time()} {epoch:3}: {log['reconstruction loss']:9.5f}, kl: {log['kl divergence']:15.5f} x {log['kl annealing']:.3f}"
                
                log["co-intent weight"] = att_cointent_weight(epoch)
                message += ", co-intent loss: "
                log["co-intent metric loss"] = sum(att_cointent_losses)/len(att_cointent_losses) if len(att_cointent_losses) > 0 else 'NaN'
                if log["co-intent metric loss"] != 'NaN':
                    message += f"{log['co-intent metric loss']:11.5f} x {log['co-intent weight']:4.3f}"
                else:
                    message += " "*18
                
                log["size weight"] = size_weight(epoch)
                message += ", size loss: "
                log["size metric loss"] = sum(sizes_losses)/len(sizes_losses) if len(sizes_losses) > 0 else 'NaN'
                if log["size metric loss"] != 'NaN':
                    message += f"{log['size metric loss']:11.5f} x {log['size weight']:4.3f}"
                else:
                    message += " "*18
                    
                print(message)
                
            logs.append(log)
            logs.append(dev_performance(epoch))
            data["epoch logs"] = logs
            torch.save(data, name + '.tch')

    torch.save(data, name + '.tch')


    # %%
    accuracies, row_accuracy, cell_accuracy = [], [], []
    aucs = []
    from sklearn.metrics import roc_auc_score
    with torch.no_grad():
        for j in range(1,10):
            print(f"--- {j/10} ---")
            for contexts, intents, stops, links, sizes in eval_dataloader:
                a_emb, mu, logvar = encoder.cpu()(contexts)
                o_emb = encoder.encode_objects(contexts, mu)
                pred = decoder.cpu()(mu, o_emb)

                for i in range(len(pred)):
                    try:
                        auc = roc_auc_score(contexts[i].view(-1), pred[i].view(-1))
                        aucs.append(auc)
                    except ValueError:
                        pass

                accuracy = (contexts > 0) == (pred > j/10) # compare the cells
                cell_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)
                row_accuracy.append(accuracy.cpu().view(-1))
                accuracy = accuracy.all(dim=-1)  # check for each sample in the batch if all its cells wwere correctly predicted
                accuracies.append(accuracy.cpu())
            print(f"accuracy (rate of contexts sets fully well-predicted): {torch.cat(accuracies, dim=0).type(torch.float).mean()}")
            print(f"row_accuracy: {torch.cat(row_accuracy).type(torch.float).mean()}")
            print(f"cell_accuracy: {torch.cat(cell_accuracy).type(torch.float).mean()}")
            print(f"AUC ROC average: {sum(aucs)/len(aucs):.5f}")
            break


        import matplotlib.pyplot as plt
        for contexts, intents, stops, links, sizes in eval_dataloader:
            a_emb, mu, logvar = encoder.cpu()(contexts)
            o_emb = encoder.encode_objects(contexts, mu)
            pred = decoder.cpu()(mu, o_emb)

            f = plt.figure(figsize=(10, len(pred) * 2))
            for i, (prediction, sample) in enumerate(zip(pred.cpu(), contexts)):
                f.add_subplot(3, len(pred), i+1)
                plt.imshow(prediction.detach().cpu().numpy(), vmin=0, vmax=1)

                # f.add_subplot(3, len(pred), i+len(pred)+1)
                # plt.imshow(prediction.detach().round().cpu().numpy(), vmin=0, vmax=1)

                f.add_subplot(3, len(pred), i+len(pred)+len(pred)+1)
                plt.imshow(sample.cpu().numpy(), vmin=0, vmax=1)
            plt.savefig(name + '.png')
            break
