# FCAt


This repository contains the code of Formal Concept Attention-based generator (FCAt).
This is still a work in progress.

It is the result of the Master 2 internship work of Esteban Marquer, under the supervision of Miguel Couciero and Ajinkya Kulkarni, with the help of Alain Gely and Alexandre Bazin.

Our experiments were carried out using the Grid'5000 testbed, supported by a scientific interest group hosted by Inria and including CNRS, RENATER and several Universities as well as other organizations (see https://www.grid5000.fr).

We would like to thank the Inria Project Lab (IPL) HyAIAI ("Hybrid Approaches for Interpretable Artificial Intelligence") for funding this project.

## Requirements
The code present in this repository is designed for Python 3.8 and is based on the PyTorch deep-learning library.
We recommend the use of Anaconda to set up the work environement.
The following two sections contain the required code to install the dependencies respectively with Anaconda and pip.

### Anaconda or Miniconda
```bash
conda create -y --name boa python==3.8
conda install -y --name boa -c conda-forge -c pytorch --file requirements.txt
conda activate boa
```
### Virtualenv, venv or pip
```bash
pip install -r requirements_pip.txt
```
