#from link_generation import *
from full_concept_gen import *
#from concept_n_link_gen_plus import *
#from concept_n_link_generation import *

device = "cpu"#"cuda" if torch.cuda.is_available() else "cpu"
batch_size = 2
batch_id = 6#3
shuffle = True
num_workers = 8

model_name = "models/concept__"
#data = torch.load("models/concept_generator_link.tch", map_location=device)
#data = torch.load("models/concept.tch", map_location=device)
data = torch.load(model_name + ".tch", map_location=device)

#skeleton_generator = data['skeleton_generator']
concept_generator = data['concept_generator']
#concept_generator.dropout.p = 0.25
skeleton_generator = data.get("skeleton_generator", None)
#decoder = data['decoder']
#adjacency_cnn_model = data['adjacency_cnn_model']
#adjacency_lin_model = data['adjacency_lin_model']

# dataset
import os
if os.path.isdir("latticenn/random-lattice-dataset/small_train/preprocessed"):
    train_dataset, dev_dataset = get_dataset("latticenn/random-lattice-dataset/small_train/preprocessed")
    eval_dataset = LatticeDataset("latticenn/random-lattice-dataset/small_test/preprocessed")
    boa_data = torch.load("models/boa.tch", map_location=device)
    size_predictor = torch.load("models/concept_upper_bound.tch", map_location=device)
else:
    train_dataset, eval_dataset = get_dataset("../../random-lattice-dataset/preprocessed")
    #train_dataset, eval_dataset = get_dataset("../random-lattice-dataset/minisample/preprocessed")
    dev_dataset = eval_dataset
    boa_data = torch.load("../boa/boa_vae_e500_lr3schedule_b0001_cointent.tch", map_location=device)
    size_predictor = torch.load("concept_upper_bound.tch", map_location=device)
collate = Collate(DataAugment(0.1), masks=False)

decoder = boa_data['decoder']
decoder.oa_decoder = decoder.oa_decoder[:-1]

# create dataloader
train_dataloader = DataLoader(train_dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        num_workers=num_workers,
                        collate_fn=collate)
dev_dataloader = DataLoader(dev_dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        num_workers=num_workers,
                        collate_fn=collate)
eval_dataloader = DataLoader(eval_dataset,
                        batch_size=batch_size,
                        shuffle=shuffle,
                        num_workers=num_workers,
                        collate_fn=collate)


# %%
# reset random seed
torch.manual_seed(0)

for i, (contexts, intents, stops, links, sizes) in enumerate(eval_dataloader):
    #if i == batch_id:#2
    contexts = contexts.to(device)
    intents = intents.to(device)
    sizes = sizes.to(device)
    links = links.to(device)

    # compute embeddings
    with torch.no_grad():
        a_emb, mu, logvar = boa_data['encoder'](contexts)
        a_emb = mu
        o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
        c_emb = boa_data['encoder'].encode_objects(intents, a_emb)
        pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
        predicted_size = size_predictor(a_emb, pred_size)
        
        if (skeleton_generator is not None):
            skeleton = skeleton_generator(a_emb, predicted_size, stops.size(1))
                
            pred_c_emb, pred_cover_adjacencies, pred_order_adjacencies = concept_generator(skeleton, a_emb, o_emb)
        else:
            pred_c_emb, pred_cover_adjacencies, pred_order_adjacencies = concept_generator(a_emb, o_emb, predicted_size, stops.size(1))
        
        pred_intents = decoder(a_emb, pred_c_emb)
        """
        a_emb, mu, logvar = boa_data['encoder'](contexts)
        a_emb = mu
        o_emb = boa_data['encoder'].encode_objects(contexts, a_emb)
        pred_size = boa_data['length_predictor'](a_emb[:,:,boa_data["sizes_slice"]])
        predicted_size = size_predictor(a_emb, pred_size)
        skeleton = skeleton_generator(a_emb, predicted_size, stops.size(1))
        c_emb = concept_generator(skeleton, a_emb, o_emb)
        pred_intents = torch.sigmoid(decoder(a_emb, c_emb))
        pred_links = torch.sigmoid(adjacency_cnn_model(pred_intents))
        pred_links_lin = torch.sigmoid(adjacency_lin_model(c_emb))"""
            
    #break

    pred_intents = torch.sigmoid(pred_intents)
    pred_cover_adjacencies = torch.sigmoid(pred_cover_adjacencies)
    pred_order_adjacencies = torch.sigmoid(pred_order_adjacencies)

    rows = batch_size
    columns = 6

    fig = plt.figure(figsize=(2*columns, 4*rows))


    def display_links(link_matrix, concepts):
        t = torch.tril_indices(concepts.size(0), concepts.size(0), offset=-1)
        links = torch.zeros(concepts.size(0), concepts.size(0), device=device)
        links[t[0, :], t[1, :]] = link_matrix
        return links

    axes = []
    for sample_id in range(batch_size):
        row = sample_id * columns
        
        #gold intents
        axes.append(fig.add_subplot(rows, columns, 1 + row))
        if sample_id == 0: axes[-1].set_ylabel("Gold Intents")
        plt.imshow(intents[sample_id], vmin=0, vmax=1)
        
        #gold links
        axes.append(fig.add_subplot(rows, columns, 2 + row))
        if sample_id == 0: axes[-1].set_ylabel("Gold Cover Relation")
        plt.imshow(display_links(links[sample_id,:,0], intents[sample_id]), vmin=0, vmax=1)
        
        axes.append(fig.add_subplot(rows, columns, 3 + row))
        if sample_id == 0: axes[-1].set_ylabel("Gold Order Relation")
        plt.imshow(display_links(links[sample_id,:,1], intents[sample_id]), vmin=0, vmax=1)
        
        # pred intent
        axes.append(fig.add_subplot(rows, columns, 4 + row))
        if sample_id == 0: axes[-1].set_ylabel("Pred Intents")
        plt.imshow(pred_intents[sample_id], vmin=0, vmax=1)
        
        # pred links lin
        axes.append(fig.add_subplot(rows, columns, 5 + row))
        if sample_id == 0: axes[-1].set_ylabel("Cover Relation")
        plt.imshow(display_links(pred_cover_adjacencies[sample_id], intents[sample_id]), vmin=0, vmax=1)
        
        # pred links CNN
        axes.append(fig.add_subplot(rows, columns, 6 + row))
        if sample_id == 0: axes[-1].set_ylabel("Order Relation")
        plt.imshow(display_links(pred_order_adjacencies[sample_id], intents[sample_id]), vmin=0, vmax=1)
    fig.tight_layout()
    plt.savefig(model_name + "small.png")
    print(i)
    if input(): break
