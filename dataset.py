import torch, torch.nn as nn, numpy as np, pickle
from torch.utils.data import Dataset, DataLoader
from pathlib import Path
import typing as t
torch.manual_seed(0)

VERBOSE = False

# %%
def load_lattice(file: str):
    """Load lattice data from the provided pickle path.

    :param file: 
        The file path of the lattice pickle.
        
    :return:
        The returned values matches what is provided to write_lattice, and are returned as a tuple.
        In order:
        :order:
            The ordering of initial concepts, which is used to encode the concept names as numbers and determine the
            order in the sequence.
        :sequence:
            The adjacency sequence.
        :object_to_concept:
            The object-to-concept vector.
        :attribute_to_concept:
            The attribute-to-concept vector.
        :extents:
            The matrix of extents of the concepts, each intent being a set of indices of the attributes in it.
        :intents:
            The matrix of intents of the concepts, each intent being a set of indices of the attributes in it.
        :context:
            The boolean matrix of the formal context.
    """
    with open(file, "rb") as f:
        data = pickle.load(f)
    object_to_concept = pickle.loads(data["object_to_concept"])
    attribute_to_concept = pickle.loads(data["attribute_to_concept"])
    extents = pickle.loads(data["extents"])
    intents = pickle.loads(data["intents"])
    context = pickle.loads(data["context"])
    return data["order"], data["sequence"], object_to_concept, attribute_to_concept, extents, intents, context

def triangularize(links, size):
    """ Transforms a flattened triangular adjacency matrix back into a matrix.

    Also handles the padding of the matrix.
    
    :param links:
        The vector of the triangular adjacency matrix.
    :param size:
        The size of the output matrix.
    :return:
        The triangular adjacency matrix, has a dimension of (size, size).
    """
    m = torch.zeros((size, size), dtype=links.dtype)

    tril_indices = torch.tril_indices(row=size, col=size, offset=-1)
    m[tril_indices[0], tril_indices[1]] = links
    return m

def detriangularize(links):
    """ Flatten a triangular adjacency matrix into a vector.
    
    :param links:
        The triangular adjacency matrix.
    :return:
        The flattened adjacency matrix, of size (batch, num_c * num_c / 2).
    """
    tril_indices = torch.tril_indices(row=links.size(-1), col=links.size(-1), offset=-1)
    return torch.stack([links[x, y] for x, y in tril_indices.t()])

def transitivity(triangular_matrix: torch.Tensor):
    """ Computes the transitive closure of a (triangular) adjacency matrix.
    
    :param triangular_matrix:
        The (triangular) adjacency matrix.
    :return:
        The transitive closure of the adjacency matrix.
    """
    previous_step = torch.zeros_like(triangular_matrix)
    current_step = triangular_matrix
    while (current_step != previous_step).any():
        previous_step = current_step
        current_step = ((current_step + torch.matmul(current_step, triangular_matrix)) > 0).type(triangular_matrix.dtype)
    return current_step

class LatticeDataset(Dataset):
    """ A dataset of lattices, with a main folder and all its sub-folders included. """
    root: Path
    files: t.List[Path]

    def __init__(self, folder):
        """ Creates a lattice dataset.
        
        :param folder:
            The file path of the root folder.
            Can be relative or absolute.
        """
        self.root = Path(folder)
        self.files = [file for file in self.root.rglob('*.pkl')]

    def __len__(self) -> int:
        return len(self.files)

    def __getitem__(self, i):
        order, sequence, object_to_concept, attribute_to_concept, extents, intents, context = load_lattice(self.files[i])
        return context, intents, sequence

    def __str__(self) -> str:
        return f"{self.__class__.__name__} object of {len(self)} lattices; bound to folder '{self.root}'"


def get_dataset(path="dataset/train", train_ratio = 0.9):
    """ Load and split a dataset into train and dev datasets based on the given ratio.
    
    :param path:
        The file path of the root folder.
        Can be relative or absolute.
    :param train_ratio:
        The portion of the data dedicated to the training set.
        Ex: 0.9 for 90% of the samples in the training set and the rest in the development set.
    :return:
        The train and dev sets.
    """
    # reset random seed
    torch.manual_seed(0)

    # load dataset
    dataset = LatticeDataset(path)

    # split dataset
    train_split = int(len(dataset) * train_ratio)
    train_dataset, dev_dataset = torch.utils.data.random_split(dataset, [train_split, len(dataset) - train_split])
    return train_dataset, dev_dataset

def pad_2D(matrix, target_row_number, target_column_number, masks=False):
    """ Padding process for matrices.
    
    The pading value is 0.

    :param matrix:
        The matrix to pad.
    :param target_row_number:
        The number of rows wanted.
    :param target_column_number:
        The number of columns wanted.
    :param masks:
        If true, will return a boolean mask with False where the output corresponds to padding and True otherwise.
    :return:
        The 0-padded matrix, of size (target_row_number, target_column_number).
        If masks is True, also returns the boolean mask, of size (target_row_number, target_column_number).
    """
    missing_rows = target_row_number - len(matrix)
    missing_columns = target_column_number - len(matrix[0])

    padded_matrix = nn.functional.pad(matrix, (0, missing_columns, 0, missing_rows), value=False)
    if masks:
        mask = torch.zeros(target_row_number, target_column_number, dtype=torch.bool)
        mask[:len(matrix), :len(matrix[0])] = True
        return padded_matrix, mask
    else:
        return padded_matrix

def pad_1D(vector, target_length, masks=False):
    """ Padding process for vector.
    
    The pading value is 0.

    :param vector:
        The vector to pad.
    :param target_length:
        The length wanted.
    :param masks:
        If true, will return a boolean mask with False where the output corresponds to padding and True otherwise.
    :return:
        The 0-padded matrix, of size (target_row_number, target_column_number).
        If masks is True, also returns the boolean mask, of size (target_row_number, target_column_number).
    """
    missing_length = target_length - len(vector)

    padded_vector = nn.functional.pad(vector, (0, missing_length), value=False)
    if masks:
        mask = torch.zeros(target_length, dtype=torch.bool)
        mask[:len(vector)] = True
        return padded_vector, mask
    else:
        return padded_vector


class DataAugment:
    """ Data augmentation process, with a duplication, shuffling and random drop. """
    def __init__(self, duplicate_rate=0.3, drop_rate=0.01):
        """ Creates a data augmentation process object.
        
        :param duplicate_rate:
            Probability for each element to be duplicated. Use 0 to disable duplication.
        :param drop_rate:
            Probability for entry element to be dropped (inverted). Use 0 to disable dropping.
        """
        self.duplicate_rate = duplicate_rate
        if self.duplicate_rate > 0:
            self.geom = torch.distributions.geometric.Geometric(torch.tensor(1-duplicate_rate))
        self.drop_rate = drop_rate

    def duplicate(self, contexts, intents):
        """ Applies the random duplication process on contexts and intents matrices.
        
        :param contexts:
            Formal context matrix, of size either (num_o, num_a) or (batch, num_o, num_a).
        :param intents:
            Intents matrix, of size either (num_c, num_a) or (batch, num_c, num_a).
        :return:
            The contexts and intents matrices with duplicated objects and attributes.
        """
        num_o, num_a = contexts.size()[-2:]
        # attribute duplication, using a geometric distribution to emulate the repeatable duplication.
        attribute_duplication_amount = self.geom.sample((num_a,))
        attribute_indices = [i for i in range(num_a) for j in range(attribute_duplication_amount[i].int().item()+1)]

        # object duplication
        object_duplication_amount = self.geom.sample((num_o,))
        object_indices = [i for i in range(num_o) for j in range(object_duplication_amount[i].int().item()+1)]

        if contexts.dim() == 2:
            return contexts[object_indices][:, attribute_indices], intents[:, attribute_indices]
        elif contexts.dim() == 3:
            return contexts[:, object_indices][:, :, attribute_indices], intents[:, :, attribute_indices]

    def drop(self, contexts):
        """ Applies the drop (random invertion) process on contexts.
        
        :param contexts:
            Formal context matrix, of size either (num_o, num_a) or (batch, num_o, num_a).
        :return:
            The context matrix with randomely inverted values.
        """
        drop = torch.rand(*contexts.size()) < self.drop_rate

        return (contexts.bool() ^ drop).type(contexts.dtype)

    def shuffle(self, contexts, intents):
        """ Applies the shuffling process on contexts and intents matrices.
        
        :param contexts:
            Formal context matrix, of size either (num_o, num_a) or (batch, num_o, num_a).
        :param intents:
            Intents matrix, of size either (num_c, num_a) or (batch, num_c, num_a).
        :return:
            The contexts and intents matrices with shuffled objects and attributes.
        """
        num_o, num_a = contexts.size()[-2:]
        attribute_perm = torch.randperm(num_a)
        object_perm = torch.randperm(num_o)
        if contexts.dim() == 2:
            return contexts[object_perm][:, attribute_perm], intents[:, attribute_perm]
        elif contexts.dim() == 3:
            return contexts[:, object_perm][:, :, attribute_perm], intents[:, :, attribute_perm]

    def __call__(self, contexts, intents):
        """ Applies the full data augmentation pipeline on contexts and intents matrices.
        
        :param contexts:
            Formal context matrix, of size either (num_o, num_a) or (batch, num_o, num_a).
        :param intents:
            Intents matrix, of size either (num_c, num_a) or (batch, num_c, num_a).
        :return:
            The contexts and intents matrices with duplicated and shuffled objects and attributes,
            and inverted values in the context.
        """
        if self.duplicate_rate > 0:
            contexts, intents = self.duplicate(contexts, intents)
        contexts, intents = self.shuffle(contexts, intents)
        if self.drop_rate > 0:
            contexts = self.drop(contexts)
        return contexts, intents

# collate object
class Collate:
    """ Collate object for pre-processing the loaded data. """
    def __init__(self, data_augment=None, masks=False, direct_links=True):
        """ Creates a new collate object.
        
        The pading value is 0.

        :param data_augment:
            If provided, this data augmentation process will be applied.
        :param masks:
            If true, will return a boolean mask with False where the output corresponds to padding and True otherwise,
            for the contexts, intents and links.
        :param direct_links:
            If true, will return two dimensions for the link vectors: transitive reduction and transitive closure. Otherwise, will return only the transitive closure.
        """
        self.masks = masks
        self.direct_links = direct_links
        self.data_augment = data_augment

    def __call__(self, batch):
        """ Pre-processes a batch of loaded data.

        :param batch:
            The batch of data to preprocess.
        
        :return:
            The preprocessed batch of data, as a tuple.
            In order:
            :contexts:
                The contexts matrix, of size (batch, num_o, num_a).
            :intents:
                The intents matrix, of size (batch, num_c, num_a).
            :stops:
                The stops matrix, of size (batch, num_c).
                The elements are 0 if the corresponding concept is not the last and 1 if it is.
            :links:
                The flattened adjacency matrix, of size (batch, num_c * num_c / 2). If transitive_links==True in the constructor, the size is (batch, num_c * num_c / 2, 2).
            :sizes:
                The number of concepts vector, of size (batch).
            :c_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the contexts matrix corresponds to padding and True otherwise.
            :i_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the intents matrix corresponds to padding and True otherwise.
            :l_mask:
                Only present if masks==True in the constructor.
                A boolean mask with False where the flattened adjacency matrix corresponds to padding and True otherwise.
        """
        # sample processing to torch data
        samples = []
        for sample_context, sample_intents, sample_sequence in batch:
            # context
            sample_context = torch.from_numpy(sample_context).type(torch.float)

            # concept intents
            sample_intents = torch.from_numpy(sample_intents).t().type(torch.float)

            # augment context and intents
            if self.data_augment is not None:
                sample_contexts, sample_intents = self.data_augment(sample_context, sample_intents)

            # concept links
            if len(sample_sequence) > 1:
                sample_links = [torch.Tensor(link) for link in sample_sequence[1:]]
                sample_links = torch.cat(sample_links)
                
                if self.direct_links:
                    sample_links = torch.stack([
                        sample_links, # transitive reduction
                        detriangularize(transitivity(triangularize(sample_links, len(sample_intents)))) # transitive reduction
                    ], dim = -1)
                else:
                    sample_links = detriangularize(transitivity(triangularize(sample_links, len(sample_intents))))
                    
            else:
                if VERBOSE: print(f"sample with no links, {len(sample_intents)} concept: {sample_intents}, \nsample = {sample_context.numpy()}")
                sample_links = torch.zeros(1)

            samples.append((sample_context, sample_intents, sample_sequence, sample_links))
        I = []
            
        # pad all the data
        max_attributes = max(len(context[0]) for context, intents, sequence, links in samples)
        max_objects = max(len(context) for context, intents, sequence, links in samples)
        max_concepts = max(len(intents) for context, intents, sequence, links in samples)
        max_links = max(len(links) for context, intents, sequence, links in samples)
        contexts, intents, links = [], [], []
        c_mask, i_mask, l_mask = [], [], []
        for sample_context, sample_intents, sample_sequence, sample_links in samples:
            if sample_links.dim() < 2: # patch
                sample_links = torch.stack([sample_links]*2)
            
            # context
            sample_context, mask = pad_2D(sample_context, max_objects, max_attributes, masks=True)
            contexts.append(sample_context)
            c_mask.append(mask)
            # concept intents
            sample_intents, mask = pad_2D(sample_intents, max_concepts, max_attributes, masks=True)
            intents.append(sample_intents)
            i_mask.append(mask)
            # concept links
            if self.direct_links:
                sample_links, mask = pad_2D(sample_links, max_links, 2, masks=True)
                links.append(sample_links)
            else:
                sample_links, mask = pad_1D(sample_links, max_links, masks=True)
                links.append(sample_links)
            l_mask.append(mask)

        # stop matrix
        sizes = torch.tensor([len(intents[0]) - 1 for context, intents, links in batch])
        arange = torch.arange(max_concepts)
        aranges = torch.stack([arange] * len(batch))
        stops = (aranges == torch.stack([sizes] * max_concepts, dim=-1)).type(torch.long)

        # aggregate the lists into tensors
        contexts = torch.stack(contexts)
        intents = torch.stack(intents)
        links = torch.stack(links)
        if self.masks:
            c_mask = torch.stack(c_mask)
            i_mask = torch.stack(i_mask)
            l_mask = torch.stack(l_mask)
            return contexts, intents, stops, links, sizes, c_mask.bool(), i_mask.bool(), l_mask.bool()
        else:
            return contexts, intents, stops, links, sizes


if __name__ == "__main__":
    collate = Collate(DataAugment())
    # create dataloader
    train_dataset, eval_dataset = get_dataset("../random-lattice-dataset/preprocessed")
    train_dataloader = DataLoader(train_dataset,
                            batch_size=4,
                            shuffle=False,
                            num_workers=0,
                            collate_fn=collate,
                            pin_memory=True)
    for contexts, intents, stops, links, sizes in train_dataloader:
        print(contexts, intents, stops, links, sizes)
        break
