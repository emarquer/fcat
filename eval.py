import torch
import torch.nn as nn
import numpy as np
import pandas as pd
from sklearn.metrics import roc_curve, roc_auc_score


def to_set_of_sets(matrix: torch.Tensor):
    num_c, num_a = matrix.size()
    return frozenset(
        frozenset(a for a in range(num_a) if matrix[c,a])
        for c in range(num_c)
    )

def precision_recall_f1_miss_false_discovery(pred_concepts: torch.Tensor, gold_concepts: torch.Tensor):
    if len(pred_concepts.size()) == 2:
        pred_set, gold_set = to_set_of_sets(pred_concepts), to_set_of_sets(gold_concepts)
        tp = len(pred_set.intersection(gold_set))
        fp = len(pred_set.difference(gold_set))
        fn = len(gold_set.difference(pred_set))

        precision = tp/len(pred_set)
        recall = tp/len(gold_set)
        f1 = (precision * recall) / (precision + recall) if (precision + recall) > 0 else 0
        miss_rate = fn/len(gold_set)
        false_discovery_rate = fp/len(pred_set)

        return {"precision": precision, "recall": recall, "f1": f1, "miss_rate": miss_rate, "false_discovery_rate": false_discovery_rate}
    elif len(pred_concepts.size()) < 2:
        print("Not enough dimensions")
    else:
        return [precision_recall_f1_miss_false_discovery(pred_concepts[i], gold_concepts[i]) for i in range(pred_concepts.size(0))]

def multi_threshold_scores(pred_concepts: torch.Tensor, gold_concepts: torch.Tensor, thresholds=[i/10 for i in range(0,11)]):
    if len(pred_concepts.size()) == 2:
        return [
            {"threshold": threshold, **precision_recall_f1_miss_false_discovery(pred_concepts > threshold, gold_concepts)}
            for threshold in thresholds]
    elif len(pred_concepts.size()) < 2:
        print("Not enough dimensions")
    else:
        full_list = []
        for i in range(pred_concepts.size(0)):
            full_list += multi_threshold_scores(pred_concepts[i], gold_concepts[i], thresholds)
        return full_list 

def find_optimal_threshold(predicted, target):
    """ Find the optimal probability cutoff point for a classification model related to event rate.

    From https://stackoverflow.com/a/32482924.
    
    Parameters
    ----------
    target : Matrix with dependent or target data, where rows are observations

    predicted : Matrix with predicted data, where rows are observations

    Returns
    -------     
    list type, with optimal cutoff value
        
    """
    #target = torch.clamp(target.view(-1).cpu(), 0, 1)
    target = target.view(-1).cpu().numpy()
    predicted = predicted.view(-1).cpu().numpy()
    #print(target.min(), target.max())
    #print(predicted.min(), predicted.max())
    #predicted = torch.clamp(predicted.view(-1).cpu(), 0, 1)
    fpr, tpr, threshold = roc_curve(target, predicted)
    i = np.arange(len(tpr)) 
    roc = pd.DataFrame({'tf' : pd.Series(tpr-(1-fpr), index=i), 'threshold' : pd.Series(threshold, index=i)})
    roc_t = roc.iloc[(roc.tf-0).abs().argsort()[:1]]

    return list(roc_t['threshold'])[0]


def auc_roc(predicted, target):
    target = target.view(-1).cpu().numpy()
    predicted = predicted.view(-1).cpu().numpy()
    return roc_auc_score(target, predicted)
